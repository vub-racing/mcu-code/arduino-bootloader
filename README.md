# Wiring
<img src="./Arduino_bootloader_wiring.jpg" alt="drawing" height="500"/>
<img src="./Arduino_bootloader_wiring_2.jpg" alt="drawing" height="500"/>

# Programming the bootloader and fuses
## Command line method
Upload [ArduinoISP.cpp](./ArduinoISP.cpp) code to programmer with arduinoIDE or platformIO.

### Program bootloader
Open a terminal windows and use `cd` to change the directory to this repository. Then program the bootloader with:
`./avrdude -c arduinoisp -p m328p -b 19200 -e -U lock:w:0x3F:m -U flash:w:optiboot_atmega328_16MHz_115200.hex -U lock:w:0x0F:m -P COM<insert port number>`

Use `optiboot_atmega328_20MHz_125000.hex` if you use a 20MHz crystal. The 125000 stands for the upload speed baud rate (the default is 115200). For PlatformIO add `upload_speed = 125000` to the environment of in your platformio.ini file, for the arduino IDE you will need to edit the boards.txt file. If you want to use `optiboot_atmega328_20MHz_115200.hex` you will need to use `upload_speed = 113000`. This is because at 20MHz the real baud rate will be 113636 (117647 for 16MHz) and avrdude doesn't handle negative error very well.

### Set fuses
Info about the values can be found in the [ATmega328P datasheet](http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega48A-PA-88A-PA-168A-PA-328-P-DS-DS40002061A.pdf) on pages 289-293 or on http://www.engbedded.com/fusecalc.  
* Low Power Crystal Oscillator (8-16MHz for lower values see datasheet)  
`./avrdude -c arduinoisp -p m328p -b 19200 -U lfuse:w:0xFF:m -U hfuse:w:0xDE:m -U efuse:w:0x05:m -P COM<insert port number>`
* Full swing Crystal Oscillator (0.4-20MHz)  
`./avrdude -c arduinoisp -p m328p -b 19200 -U lfuse:w:0xF7:m -U hfuse:w:0xDE:m -U efuse:w:0x05:m -P COM<insert port number>`  
* 8MHz internal Oscillator (default when first bought)  
`./avrdude -c arduinoisp -p m328p -b 19200 -U lfuse:w:0xF2:m -U hfuse:w:0xDE:m -U efuse:w:0x05:m -P COM<insert port number>`  
* External Clock (0-20MHz)  
`./avrdude -c arduinoisp -p m328p -b 19200 -U lfuse:w:0xF0:m -U hfuse:w:0xDE:m -U efuse:w:0x05:m -P COM<insert port number>`  
Once the processor is programmed to use an external clock an external clock must be used to if one wants to change the clock source again. In the default settings this clock must be greater than 1MHz, but can be lowerd to 128kHz by changing [line 49](./ArduinoISP.cpp#L49) and [line 144](./ArduinoISP.cpp#L144) (lowest baud rate is 500) in [ArduinoISP.cpp](./ArduinoISP.cpp).

## Arduino IDE method (uses 16MHz Low Power Crystal Oscillator mode)
### Fix old deprecated setting
* Go to the folder where the arduino IDE is installed
* Open `.\hardware\tools\avr\etc\avrdude.conf`
* Search for `ATmega328`
* Go to block `memory "efuse"`
* Change line 
    ```
    read = "0 1 0 1 0 0 0 0 0 0 0 0 1 0 0 0",
        "x x x x x x x x o o o o o o o o";
    ```
    to
    ```
    read = "0 1 0 1 0 0 0 0 0 0 0 0 1 0 0 0",
        "x x x x x x x x x x x x x o o o";
    ```

### Program the bootloader
* Open arduino IDE
* File > Examples > 11.ArduinoISP > ArduinoISP
* Add `#define USE_OLD_STYLE_WIRING`
* Tools > Port > select correct port
* Tools > Board > Arduino/Genuino Uno
* Upload to programmer
* Tools > Programmer > Arduino as ISP
* Tools > Burn bootloader